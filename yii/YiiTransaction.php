<?php


namespace xing\helper\yii;

use yii\db\Connection;
use yii\db\Transaction;

/**
 * Class YiiTransaction
 * @package xing\helper\yii
 */
class YiiTransaction
{
    public static $transaction = [];

    /**
     * @param Connection $class
     */
    public static function addTransaction($class)
    {
        static::$transaction[] = $class->beginTransaction();
    }

    public static function commit()
    {
        foreach (static::$transaction as $db) $db->commit();
    }

    public static function rollBack()
    {
        array_reverse(static::$transaction);
        foreach (static::$transaction as $db) $db->rollBack();
        static::$transaction = [];
    }
}