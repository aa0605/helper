<?php


namespace xing\helper\yii;


trait MyTransactionTrait
{

    public static $transactions = [];

    /**
     * @param \xing\helper\yii\BaseActiveModel ...$models
     */
    public static function batchStartTransaction(...$models)
    {
        static::$transactions = [];
        static::batchAddTransaction(...$models);
    }

    /**
     * @param \xing\helper\yii\BaseActiveModel ...$models
     */
    public static function batchAddTransaction(...$models)
    {
        foreach ($models as $model) static::$transactions[] = $model::getDb()->beginTransaction();
    }

    public static function batchCommit()
    {
        foreach (static::$transactions as $v) $v->commit();
    }

    public static function batchRollBack()
    {
        foreach (static::$transactions as $v) $v->rollBack();
    }
}