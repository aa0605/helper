<?php

namespace xing\helper\yii\behavior;

class Location2jsonBehavior extends \yii\behaviors\AttributeBehavior
{
    public function evaluateAttributes($event)
    {

        if (!empty($this->attributes[$event->name])) {
            $attributes = (array) $this->attributes[$event->name];
            foreach ($attributes as $attribute) {
                if (is_string($this->owner->$attribute)) {
                    $lcation = json_decode($this->owner->$attribute, 1);
                    if (!empty($lcation['lat'] ?? null) && !empty($lcation['lon'] ?? null)) {
                        $this->owner->$attribute = $lcation;
                    }
                }
            }
        }
    }
}
