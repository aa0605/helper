<?php

namespace xing\helper\yii\behavior;

/**
 * ```php
 * use yii\db\Expression;
 *
 * public function behaviors()
 * {
 *     return [
 *           [
 *               'class' => \xing\helper\yii\behavior\DateTimeBehavior::class,
 *               'attributes' => [
 *                   ActiveRecord::EVENT_BEFORE_INSERT => ['createTime','updateTime'],
 *                   ActiveRecord::EVENT_BEFORE_UPDATE => ['updateTime'],
 *               ],
 *           ],
 *     ];
 * }
 * ```
 * 也可以直接touch某个变量
 * $model->touch('creationTime');
 */
class DateTimeBehavior extends \yii\behaviors\AttributeBehavior
{
    public $value;

    protected function getValue($event)
    {
        return date('Y-m-d H:i:s');
    }
}
