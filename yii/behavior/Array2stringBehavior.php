<?php

namespace xing\helper\yii\behavior;

use yii\base\Event;
use yii\db\ActiveRecord;

/**
 *
 */
class Array2stringBehavior extends \yii\behaviors\AttributeBehavior
{
    public $value;
    public $symbol = ',';

    /**
     * Evaluates the attribute value and assigns it to the current attributes.
     * @param Event $event
     */
    public function evaluateAttributes($event)
    {

        if (!empty($this->attributes[$event->name])) {
            $attributes = (array) $this->attributes[$event->name];
            foreach ($attributes as $attribute) {
                if (is_array($this->owner->$attribute)) {
                    $this->owner->$attribute = implode($this->symbol, $this->owner->$attribute);
                }
            }
        }
    }
}
